import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class InterestInfo extends StatelessWidget {
    
    final String interest;
    final IconData icon;

    InterestInfo(this.interest, this.icon);

    @override
    Widget build(BuildContext context) {
        return ListTile(
            leading: Padding(
                padding: EdgeInsets.only(
                    top: 8, 
                    left: 20
                ),
                child: Icon(
                    icon, 
                    size:20,
                    color:  Color.fromRGBO(20, 45, 68, 1)
                )
            ),
            title: Text(
                interest,
                style: TextStyle(
                    fontFamily: 'Raleway',
                    color:  Color.fromRGBO(20, 45, 68, 1),
                    fontWeight: FontWeight.bold
                )
            ),  
        );
    }
}