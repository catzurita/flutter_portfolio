import 'package:flutter/material.dart';

class TitleInfo extends StatelessWidget {
  
    final String title;

    TitleInfo(this.title);

    @override
    Widget build(BuildContext context) {
        return Padding( 
            padding: EdgeInsets.only(left: 16.0),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                    Text(
                        title.toUpperCase(),
                        style: TextStyle(
                            fontFamily: 'Raleway',
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                            color:  Color.fromRGBO(20, 45, 68, 1)
                        ),
                    ),
                    Divider(
                        indent: 0,
                        endIndent: 10,
                        color:  Color.fromRGBO(20, 45, 68, 1),
                        thickness: 3,
                    )
                ],
            )
        );
    }
}
