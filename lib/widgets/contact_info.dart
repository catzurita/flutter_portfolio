import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class ContactInfo extends StatelessWidget {

    IconButton addIcon(Widget icon){
        return IconButton(
                    color:  Color.fromRGBO(20, 45, 68, 1),
                    icon: icon,
                    onPressed: () => {},
        );
    }
    @override
    Widget build(BuildContext context) {
        return Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
                addIcon(Icon(FontAwesomeIcons.linkedin)),
                SizedBox(width: 5,),
                addIcon(Icon(FontAwesomeIcons.gitlab)),
                SizedBox(width: 5,),
                addIcon(Icon(FontAwesomeIcons.facebook)),
                SizedBox(width: 5,),
                addIcon( Icon(FontAwesomeIcons.instagram)),
            ],
        );
    }
}