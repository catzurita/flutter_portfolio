import 'package:flutter/material.dart';

class SkillsInfo extends StatelessWidget {

    final String skill;
    final double level;

    SkillsInfo(this.skill, this.level);

    @override
    Widget build(BuildContext context) {
        return Row(
            children: [
                SizedBox(width: 14,),
                Expanded(
                    flex: 2,
                    child: Text(
                        skill.toUpperCase(),
                        textAlign: TextAlign.right,
                        style: TextStyle(
                            fontFamily: 'Raleway',
                            color:  Color.fromRGBO(20, 45, 68, 1)
                        ),
                    )
                ),
                SizedBox(width: 10),
                Expanded(
                    flex: 5,
                    child: LinearProgressIndicator(
                        backgroundColor: Colors.white,
                        color: Color.fromRGBO(205, 23, 25, 1),
                        value: level
                    )
                ),
                SizedBox(width: 16,)
            ],
        );
    }
}