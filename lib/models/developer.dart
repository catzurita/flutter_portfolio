import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../widgets/skills_info.dart';
import '../widgets/experience_info.dart';
import '../widgets/interest_info.dart';
import '../widgets/title_info.dart';

class Developer{

    List<Widget> get listSkills{
        return [
            TitleInfo("Skills"),
            SizedBox(height:5),
            SkillsInfo('Javascript', 0.8),
            SkillsInfo('CSS', 0.6),
            SkillsInfo('Html', 0.7),
            SkillsInfo('ExpressJS', 0.6),
            SkillsInfo('ReactJS', 0.75),
            SkillsInfo('Dart', 0.6),
            SkillsInfo('Flutter', 0.7)
        ];
    }

    List<Widget> get listExperience{
        return [
            TitleInfo("Experience"),
            ExperienceInfo(
                company: "Taikisha Philippines Inc",
                position: "Electronics/Electrical Site Engineer",
                duration: "2019-2021" 
            ),
            ExperienceInfo(
                company: "Trend Micro Philippines",
                position: "Core Technology Engineer",
                duration: "2018-2019" 
            )
        ];
    }

    List<Widget> get listInterest{
        return [
            TitleInfo('Interest'),
            InterestInfo('Game Development', FontAwesomeIcons.gamepad),
            InterestInfo('Cisco Networking', FontAwesomeIcons.networkWired),
            InterestInfo('Artificial Intelligence', FontAwesomeIcons.robot),
        ];
    }


}